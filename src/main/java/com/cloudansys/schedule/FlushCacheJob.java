package com.cloudansys.schedule;

import com.cloudansys.core.cache.CacheManager;
import com.cloudansys.core.constant.Const;
import com.cloudansys.core.util.JedisUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
class FlushCacheJob implements Runnable {

    /**
     * 定时刷新缓存
     */
    @Override
    public void run() {
//        log.info("----<FlushCacheJob>----");
        try {
            String flagTimeInRedis = JedisUtil.get(Const.HK_TASK_INIT);
            if (flagTimeInRedis != null) {
                String flagTimeInCache = CacheManager.getFlagTimeInCache();
                // 判断 redis 中的标志时间和缓存中的标志时间是否相同，相同代表不需要刷新缓存
                if (flagTimeInRedis.equals(flagTimeInCache)) {
                    return;
                }
                // 刷新缓存
                CacheManager.flushCache();
                // 把缓存中的标志时间和 redis 中的同步
                CacheManager.setFlagTimeInCache(flagTimeInRedis);
                log.info("##### flush cache success #####");
            }
        } catch (Exception e) {
            log.error("##### flush cache failed #####");
            log.error("errMsg: " + e.getMessage());
            e.printStackTrace();
        }
    }

}
