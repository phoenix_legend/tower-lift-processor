package com.cloudansys.schedule;

import cn.hutool.cron.CronUtil;

/**
 * 定时任务调度
 *
 * @author wlf
 */
public class MainScheduler {

    /**
     * 配置定时任务
     */
    public static void start() {
        // 配置需要执行的定时任务
        configScheduleJob();
        // 设置支持秒级别定时任务
        CronUtil.setMatchSecond(true);
        // 启动
        CronUtil.start();
    }

    /**
     * 基于 cron 表达式配置定时任务
     * 配置需要执行的定时任务
     */
    private static void configScheduleJob() {
        // 【每隔 10 秒】定时刷新缓存，和 redis 缓存保持同步
        CronUtil.schedule("*/10 * * * * *", new FlushCacheJob());

        // 【每隔 10 秒】定时把告警信息写入到 MySQL 数据库中
        CronUtil.schedule("*/10 * * * * *", new SaveWarnJob());
    }

}
