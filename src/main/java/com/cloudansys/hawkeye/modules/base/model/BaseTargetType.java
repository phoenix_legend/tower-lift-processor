package com.cloudansys.hawkeye.modules.base.model;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BaseTargetType implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private String tag;
    private String name;
    private String unit;
    private String remark;

}
