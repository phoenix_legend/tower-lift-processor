package com.cloudansys.hawkeye.modules.settings.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrimaryWarn implements Serializable {

    private static final long serialVersionUID = 1L;

    private Date time;
    private Integer level;
    private Integer projectId;
    private Integer targetId;
    private String targetCode;
    private Integer targetTypeId;
    private Integer serialNumber;
    private String message;
    private String dataTime;
    private Double dataValue;
    private Integer quotaId;
    private Integer quotaCode;
    private String quotaName;
    private String quotaUnit;
    private Integer thresholdId;
    private String thresholdName;
    private Double lower;
    private Double upper;
    private Boolean absolute;
    private Double[] rawData;
    private Double currentHeight;

}
