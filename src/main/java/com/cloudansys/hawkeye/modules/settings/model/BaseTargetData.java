package com.cloudansys.hawkeye.modules.settings.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseTargetData implements Serializable {

    private static final long serialVersionUID = 1L;

    // targetId 测点id
    private Integer id;

    // 测点数据时间
    private String time;

    // 测点数据
    private Double[] data;

}
