package com.cloudansys.hawkeye.modules.settings.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 监测指标
 * </p>
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SettingsDataQuota implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer targetTypeId;
    private Integer code;
    private String name;
    private String unit;
    private Integer display;

}
