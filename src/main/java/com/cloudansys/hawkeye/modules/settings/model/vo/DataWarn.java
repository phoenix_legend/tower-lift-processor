package com.cloudansys.hawkeye.modules.settings.model.vo;

import java.util.Date;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 告警对象---数据库中存储
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataWarn implements Serializable {

    private static final long serialVersionUID=1L;

    private Integer id;

    // 测点ID
    private Integer targetId;

    // 测点类型id
    private Integer targetTypeId;

    // 数据信息
    private String dataJson;

    // 阈值信息
    private String thresholdJson;

    // 数据时间
    private Date timeO;

    // 入库时间
    private Date timeW;

    // 监测指标id
    private Integer quotaId;

    // 告警等级
    private Integer level;

    // 当前告警状态 1: 已处理  0: 未处理
    private Integer status;

    // 信息
    private String message;

}

