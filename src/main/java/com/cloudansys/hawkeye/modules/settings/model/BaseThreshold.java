package com.cloudansys.hawkeye.modules.settings.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BaseThreshold implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer targetId;
    private Integer quotaId;
    private Integer quotaCode;
    private String quotaName;
    private String quotaUnit;
    private Double dataValue;
    private String name;
    private Integer level;
    private Double lower;
    private Double upper;
    private Boolean absolute;

}
