package com.cloudansys.core.util;

import com.cloudansys.config.DefaultConfig;
import com.cloudansys.core.constant.Const;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class JedisUtil {

    /**
     * 【字符串】
     * 获取指定 key 的 value
     */
    public static void set(String key, String value) {
        Jedis jedis = null;
        try {
            jedis = DefaultConfig.getJedis();
            jedis.set(key, value);
        } catch (Exception e) {
            log.error("errMsg: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * 【字符串】
     * 获取指定 key 的 value
     */
    public static String get(String key) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = DefaultConfig.getJedis();
            value = jedis.get(key);
        } catch (Exception e) {
            log.error("errMsg: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return value;
    }

    /**
     * 【list】
     * 移出并获取列表的第一个元素
     */
    public static String lPop(String key) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = DefaultConfig.getJedis();
            value = jedis.lpop(key);
        } catch (Exception e) {
            log.error("errMsg: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return value;
    }

    /**
     * 【list】
     * 通过索引获取列表中的元素
     */
    public static String lIndex(String key, int index) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = DefaultConfig.getJedis();
            value = jedis.lindex(key, index);
        } catch (Exception e) {
            log.error("errMsg: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return value;
    }

    /**
     * 【list】
     * 通过索引获取列表中的元素
     */
    public static List<String> lRange(String key, int start, int end) {
        List<String> values = null;
        Jedis jedis = null;
        try {
            jedis = DefaultConfig.getJedis();
            values = jedis.lrange(key, start, end);
        } catch (Exception e) {
            log.error("errMsg: " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return values;
    }

    /**
     * 获取存储在 redis 中的告警信息的 key
     */
    public static String getWarnKey() {
        return formatSmKey(getItemList());
    }

    /**
     * 获取构成 redis key 的元素列表
     */
    private static List<String> getItemList() {
        List<String> list = new ArrayList<>();
        list.add(Const.HK);
        list.add(Const.SM);
        list.add(Const.REDIS_CACHE_WARNS);
        return list;
    }

    /**
     * sec-mean key of redis
     * @param items [hawkkeye:secmean:传感器id]
     * @return hk:sm:ti
     */
    public static String formatSmKey(List<String> items) {
        StringBuilder buffer = new StringBuilder();
        int size = items.size();
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                buffer.append(items.get(i));
            } else {
                buffer.append(items.get(i)).append(Const.COLON);
            }
        }
        return buffer.toString();
    }

    /**
     * base-info key of redis
     * @param items [hawkkeye:baseinfo:项目id_传感器通讯编号]
     * @return hk:bi:1_1
     */
    public static String formatBiKey(List<String> items) {
        StringBuilder buffer = new StringBuilder();
        int size = items.size();
        for (int i = 0; i < size; i++) {
            if (i == size - 1) {
                buffer.append(items.get(i));
            } else if (i == size - 2) {
                buffer.append(items.get(i)).append(Const.BAR_BOTTOM);
            } else {
                buffer.append(items.get(i)).append(Const.COLON);
            }
        }
        return buffer.toString();
    }

}
