package com.cloudansys.core.util;

import com.cloudansys.core.constant.Const;

import java.util.UUID;

public class TextUtil {

    public static String randomUUID(int length) {
        return UUID.randomUUID().toString().replaceAll(Const.BAR_MID, Const.NOTHING).substring(0, length);
    }

    public static String randomInt(int range) {
        int number = (int) (Math.random() * range);
        return String.valueOf(number);
    }

}
