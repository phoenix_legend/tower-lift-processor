package com.cloudansys.core.util;

import com.alibaba.fastjson.JSONObject;
import com.cloudansys.core.constant.Const;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StrUtil {

    /**
     * 判断 string 字符串是不是 json 格式
     */
    public static boolean isJson(String content) {
        if(StringUtils.isEmpty(content)){
            return false;
        }
        boolean isJsonObject = true;
        boolean isJsonArray = true;
        try {
            JSONObject.parseObject(content);
        } catch (Exception e) {
            isJsonObject = false;
        }
        try {
            JSONObject.parseArray(content);
        } catch (Exception e) {
            isJsonArray = false;
        }
        //不是json格式
        return isJsonObject || isJsonArray;
    }

    /**
     * @return 返回底杠分隔的字符串
     */
    public static String stringsToStr(String delimiter, String... strings) {
        StringBuilder buffer = new StringBuilder();

        for (String string : strings) {
            buffer.append(string).append(delimiter);
        }
        buffer.deleteCharAt(buffer.lastIndexOf(delimiter));
        return buffer.toString();
    }

    /**
     * @param data 数组
     * @return 返回逗号分隔的字符串
     */
    public static String arrToStr(String[] data) {
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            if (i == data.length - 1) {
                buffer.append(data[i]);
            } else {
                buffer.append(data[i]).append(",");
            }
        }
        return buffer.toString();
    }

    /**
     * 返回逗号分隔的字符串
     *
     * @param mapVal sensorIds and secMeanValues of each quota 传感器 id 和各个指标的秒级均值
     * @param strVal projectId,timestamp,typeSymbol,sensorNum,quotaNum 按顺序传入
     * @return projectId,timestamp,typeSymbol,sensorNum,quotaNum,sensorId1,value1,sensorId2,value2, ...
     */
    public static String objToStr(Map<Integer, List<Double>> mapVal, Object... objects) {
        StringBuilder buffer = new StringBuilder();
        for (Object obj : objects) {
            buffer.append(obj).append(Const.COMMA);
        }

        for (Integer sensorId : mapVal.keySet()) {
            buffer.append(sensorId).append(Const.COMMA);
            List<Double> quotasSecMeanValue = mapVal.get(sensorId);
            for (Double quotaSecMeanValue : quotasSecMeanValue) {
                buffer.append(quotaSecMeanValue).append(Const.COMMA);
            }
        }
        return buffer.toString();
    }

    /**
     * @param projectId 项目id
     * @param typeSymbol 传感器类型
     * @param timestamp 数据时间
     * @param mapVal {传感器：[指标数值]}
     * @return {传感器+传感器类型+传感器+数据时间：[数值]}
     */
    public static Map<String, List<Double>> formatElement(int projectId, int typeSymbol, long timestamp, Map<Integer, List<Double>> mapVal) {
        Map<String, List<Double>> resMap = new HashMap<>();
        StringBuilder buffer = new StringBuilder();

        return resMap;
    }

}
