package com.cloudansys.core.util;

import com.cloudansys.core.constant.Const;

import java.util.ArrayList;
import java.util.List;

public class NumUtil {

    /**
     * 获取指定范围的一组数
     * @param start 起始数值
     * @param end 终止数值
     * @param origin 原始数值
     * @return 返回一组数据
     */
    public static List<Integer> getList(int start, int end, int origin) {
        List<Integer> resList = new ArrayList<>();
        for (int i = start; i <= end; i++) {
            resList.add(i + origin);
        }
        return resList;
    }

    /**
     * 把 double 类型的数值取五位小数
     */
    public static double formatDouble(Double doubleValue) {
        return Double.parseDouble(String.format(Const.FORMAT_DOUBLE, doubleValue));
    }

    /**
     * 获取两个数据的绝对值之和
     */
    public static double getSumOfAbs(double x, double y){
        return Math.abs(x) + Math.abs(y);
    }

}
