package com.cloudansys.core.util;

import com.cloudansys.core.constant.Const;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class CustomDateUtil {

    private static DateTime nowTime = new DateTime();

    /**
     * @return 返回 num 分钟前的数据，格式：2020-11-17 13:10:00
     */
    public static String getMinutesAgo(int num) {
        return nowTime.minusMinutes(num).toString(Const.FMT_STD_SEC);
    }

    /**
     * @param influxTime 时间参数，格式：2020-11-17T13:10:00+08:00
     * @return 返回格式：2020-11-17 13:10:00
     */
    public static String formatInfluxTime(String influxTime) {
        return DateTime.parse(influxTime).toString(Const.FMT_STD_SEC);
    }

    /**
     * @param trimMilliTime 时间参数，格式：20201117131000111
     * @return 返回格式：2020-11-17 13:10:00.111
     */
    public static String formatToStdTime(String trimMilliTime) {
        return DateTimeFormat.forPattern(Const.FMT_TRIM_MILLI).parseDateTime(trimMilliTime).toString(Const.FMT_STD_MILLI);
    }

    /**
     * @param trimMilliTime 时间参数，格式：20201117131000111
     * @return 返回格式：2020-11-17 13:10:00
     */
    public static String trimMilliToStdSec(String trimMilliTime) {
        return DateTimeFormat.forPattern(Const.FMT_TRIM_MILLI).parseDateTime(trimMilliTime).toString(Const.FMT_STD_SEC);
    }

    /**
     * @param timestamp 时间戳，格式：1608191061000
     * @return 返回格式：2020-11-17 13:10:00
     */
    public static String getDateFromTS(String timestamp) {
        return new DateTime(Long.parseLong(timestamp)).toString(Const.FMT_STD_SEC);
    }

    /**
     * @param date 时间戳，格式：2020-11-17 13:10:00
     * @return 返回格式：1608191061000
     */
    public static long getTsFromDate(String date) {
        return DateTimeFormat.forPattern(Const.FMT_STD_SEC).parseDateTime(date).toDate().getTime();
    }

}
