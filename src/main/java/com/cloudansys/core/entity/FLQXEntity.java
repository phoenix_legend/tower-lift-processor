package com.cloudansys.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author anri
 * @date 2020/10/27 11:16
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FLQXEntity implements Serializable {

    private static final long serialVersionUID = -457566433778417297L;

    // 项目id，即提升阶段
    private String projectId;
    // 传感器id，即通讯编号
    private String serialCode;
    // 传感器编号
    private String targetCode;
    // 传感器编号缩写
    private String targetCodeAbbr;
    // 传感器类型id
    private String typeId;
    // 传感器类型标志（大写的英文简写）
    private String typeTag;
    // 传感器类型名称
    private String typeName;
    // 传感器位置
    private String site;
    // 数据时间
    private String pickTime;
    // 指标名称（每个传感器类型有多个指标）
    private String[] quotaNames;
    // 指标单位
    private String[] quotaUnits;
    // 指标数据
    private Double[] quotaValues;
    // 告警等级
    private String warnLevel;
    // 产生告警的指标id
    private String warnQuotaId;
    // 当前提升阶段和当前高度下对应的阈值范围信息
    private List<Double> thresholds;
//    private Map<Integer, Double> thresholds;

}
