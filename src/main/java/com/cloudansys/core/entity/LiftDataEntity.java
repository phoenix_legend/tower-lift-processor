package com.cloudansys.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author anri
 * @date 2020/10/27 11:16
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LiftDataEntity implements Serializable {

    private static final long serialVersionUID = -457566433778417297L;

    // 项目id
    private String projectId;
    // 传感器id
    private String serialCode;
    // 传感器编号
    private String targetCode;
    // 传感器编号缩写
    private String targetCodeAbbr;
    // 传感器类型id
    private String typeId;
    // 传感器类型标志（大写的英文简写）
    private String typeTag;
    // 传感器类型名称
    private String typeName;
    // 传感器位置
    private String site;
    // 数据时间
    private String pickTime;
    // 指标名称（每个传感器类型有多个指标）
    private String[] quotaNames;
    // 指标单位
    private String[] quotaUnits;
    // 指标数据
    private Double[] quotaValues;

}
