package com.cloudansys.core.entity.old;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TargetInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer targetId;
    private String targetType;
    private Integer[] quotaId;
    private String[] quota;
    private Integer[][] level;
    private String[][] warn;
    private Double[][] lower;
    private Double[][] upper;
    private Boolean[] absolute;

}
