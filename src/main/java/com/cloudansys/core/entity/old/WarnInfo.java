package com.cloudansys.core.entity.old;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WarnInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer targetId;
    private String targetType;
    private String warnTime;
    private List<BaseWarn> warnList;

}
