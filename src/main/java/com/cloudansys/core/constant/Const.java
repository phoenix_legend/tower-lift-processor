package com.cloudansys.core.constant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Const {

    // 振动传感器类型(加速度)
    public static final String TT_QJ = "QJ";
    // 振动传感器类型(加速度)
    public static final String TT_ZD = "JSD";
    // 应变传感器类型
    public static final String TT_YB = "YB";
    // 位移传感器类型
    public static final String TT_WY = "WY";
    // 应变参考点
    public static final List<String> YB_CK = Arrays.asList("31", "32");
    // 【redis key】 振动告警
    public static final String ZD_WARN = "zd:warn:";
    // 【redis key】 应变参考点 31
    public static final String SCS_31 = "31";
    // 【redis key】 应变参考点 32
    public static final String SCS_32 = "32";
    // 【redis key】 平整度推送数据 存储的上次的当前高度
    public static final String EVEN_LAST_TS = "lastTs";
    // 【redis key】 平整度复位数值
    public static final String EVEN_RESET = "even:reset:";
    // 【redis key】 平整度原始数据
    public static final String EVEN_RAW = "even:raw:";
    // 【redis key】 提升高度对应的平整度数据
    public static final String EVEN_TS = "even:ts:";
    // 风环境传感器类型
    public static final String TT_WE = "WE";
    // 轮毂基础高度
    public static final Double TS_BASE = 84.44;
    // 轮毂高度传感器通讯编号
    public static final String TS_SC = "73";
    // 【redis key】当前轮毂实际高度
    public static final String TS_REDIS = "ts";
    // 【redis key】上次高度传感器的监测数据
    public static final String LAST_TS_TGT = "lastTsTgtHeight";
    // 【redis key】提升高度测量传感器原始高度
    public static final String TSB_KEY = "tsb";
    // 提升高度测量传感器原始高度：2.25米
    public static final Double TSB = 2.25;
    // 【redis key】【叶片角度】
    public static final String ANGLE_BLADE = "bladeAngle";
    // 【redis key】【振动类型阈值】
    public static final String ZD_TH_MAX = "zd:th:max:";
    public static final String ZD_TH_MIN = "zd:th:min:";

    // 应变参考点 顶固和地锚所对应的传感器通讯编号
    public static final Map<String, List<String>> SCS_YB_CK = new HashMap<String, List<String>>() {
        {
            put("31", Arrays.asList("7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18"));
            put("32", Arrays.asList("19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"));
        }
    };


    // 简写
    public static final String HK = "hk";
    public static final String PID = "pid";
    public static final String SC = "sc";
    public static final String TI = "ti";
    public static final String TT = "tt";
    public static final String BI = "bi";
    public static final String SM = "sm";

    // 传感器类型与类型编号
    public static final Map<Integer, String> TARGET_TYPE_CODE = new HashMap<Integer, String>() {
        {
            put(1,"YB");
            put(2,"YBG");
            put(3,"HL");
            put(4,"QJ");
            put(5,"FY");
            put(6,"WE");
            put(7,"GJ");
            put(8,"BX");
            put(9,"LF");
            put(10,"JSD");
            put(11,"MS");
            put(12,"QX");
            put(13,"TD");
            put(14,"WY");
            put(15,"FL");
        }
    };

    // 配置文件路径
    public static final String ENV = "pEnv";
    public static final String ENV_PROP = "env.properties";
    public static final String ENV_SUFFIX = ".properties";

    // 字符串格式
    public static final String NOTHING = "";
    public static final String COMMA = ",";
    public static final String COLON = ":";
    public static final String PERIOD = ".";
    public static final String SLASH = "/";
    public static final String BAR_BOTTOM = "_";
    public static final String BAR_MID = "-";
    public static final String ZERO = "0";
    public static final String FORMAT_DOUBLE = "%.5f";

    // punctuation[ptn][标点]
    public static final String PTN_LF = "\n";
    public static final String PTN_EMPTY = "";
    public static final String PTN_SPACE = " ";
    public static final String PTN_COMMA = ",";
    public static final String PTN_COLON = ":";
    public static final String PTN_SLASH = "/";
    public static final String PTN_PERIOD = "\\.";
    public static final String PTN_ASTERISK = "*";
    public static final String PTN_BAR_MID = "-";
    public static final String PTN_BAR_BOTTOM = "_";

    // format[fmt][格式]
    private static final String FMT_DOUBLE = "%.nf";
    public static final String FMT_TRIM_SEC = "yyyyMMddHHmmss";
    public static final String FMT_TRIM_MILLI = "yyyyMMddHHmmssSSS";
    public static final String FMT_STD_MON = "yyyy-MM";
    public static final String FMT_STD_DAY = "yyyy-MM-dd";
    public static final String FMT_STD_SEC = "yyyy-MM-dd HH:mm:ss";
    public static final String FMT_STD_MILLI = "yyyy-MM-dd HH:mm:ss.SSS";


    // flink
    public static final String APPLICATION_NAME = "application.name";

    // rabbitmq
    public static final String RABBITMQ_HOST = "rabbitmq.host";
    public static final String RABBITMQ_PORT = "rabbitmq.port";
    public static final String RABBITMQ_USERNAME = "rabbitmq.username";
    public static final String RABBITMQ_PASSWORD = "rabbitmq.password";
    public static final String RABBITMQ_VIRTUAL_HOST = "rabbitmq.virtualHost";
    public static final String RABBITMQ_QUEUE_NAME = "rabbitmq.queueName";


    // influxDB
    public static final String INFLUX_SERVER_URL = "influx.serverURL";
    public static final String INFLUX_DATABASE = "influx.database";
    public static final String INFLUX_USERNAME = "influx.username";
    public static final String INFLUX_PASSWORD = "influx.password";
    public static final String INFLUX_PAGE_MILLI = "page_raw";
    public static final String INFLUX_PAGE_SEC = "page_mean";
    public static final String INFLUX_PAGE_FIELD = "count";
    public static final Integer CORE_NUM = 4;
    public static final String INFLUX_MEASUREMENT_BASE_RAW = "raw_";
    public static final String INFLUX_MEASUREMENT_BASE_MEAN = "mean_";
    public static final String INFLUX_MEASUREMENT_FIELD_BASE = "v";


    // mysql
    public static final String MYSQL_URL = "mysql.url";
    public static final String MYSQL_USERNAME = "mysql.username";
    public static final String MYSQL_PASSWORD = "mysql.password";
    public static final String MYSQL_DRIVER = "mysql.driver";


    // mqtt
    public static final String MQTT_SERVER_URIS = "mqtt.serverURIs";
    public static final String MQTT_USERNAME = "mqtt.username";
    public static final String MQTT_PASSWORD = "mqtt.password";
    public static final String MQTT_TOPIC_MAIN_YL = "mqtt.topic.main.yl";
    public static final String MQTT_TOPIC_MAIN_ZD = "mqtt.topic.main.zd";
    public static final String MQTT_TOPIC_DATA_FL = "mqtt.topic.data.fl";
    public static final String MQTT_TOPIC_DATA_XW = "mqtt.topic.data.xw";
    public static final String MQTT_TOPIC_DATA_YL = "mqtt.topic.data.yl";
    public static final String MQTT_TOPIC_DATA_PZ = "mqtt.topic.data.pz";
    public static final String MQTT_TOPIC_DATA_EVENNESS = "mqtt.topic.data.evenness";
    public static final String MQTT_TOPIC_DATA_ZD = "mqtt.topic.data.zd";
    public static final String MQTT_TOPIC_DATA_WE = "mqtt.topic.data.we";
    public static final String MQTT_TOPIC_DATA_TS = "mqtt.topic.data.ts";
    public static final String MQTT_TOPIC_WARN = "mqtt.topic.warn";
    public static final String MQTT_TOPIC_THRESHOLD = "mqtt.topic.threshold";
    public static final String MQTT_TOPIC_TH_MAX = "mqtt.topic.threshold.max";
    public static final String MQTT_TOPIC_TH_MIN = "mqtt.topic.threshold.min";


    // DingTalk
    public static final String DING_PROJECT = "ding.project";
    public static final String DING_BASE_URL = "ding.baseUrl";
    public static final String DING_TOKEN = "ding.token";
    public static final String DING_SECRET = "ding.secret";


    // Redis
    public static final String REDIS_HOST = "redis.host";
    public static final String REDIS_DB = "redis.db";
    public static final String REDIS_PORT = "redis.port";
    public static final String REDIS_PASSWORD = "redis.password";
    public static final String REDIS_CACHE_WARNS = "warns";


    // 阈值设置信息
    public static final String HK_SET_PW = "hk:set:pw";
    // 阈值设置信息 刷新标志（存储的时间戳）
    public static final String HK_TASK_INIT = "hk:task:init";
    // 秒级瞬时值
    public static final String HK_SM = "hk:sm";
    public static final String HK_SM_WARN = "hk:sm:warn";
    public static final String HK_SM_TS = "timestamp";
    public static final String HK_SM_PT = "ptime";
    public static final String HK_SM_VAL = "v";

}
