package com.cloudansys.core.cache;

import com.cloudansys.core.constant.Const;
import com.cloudansys.core.util.JedisUtil;
import com.cloudansys.core.util.SerializeUtil;
import com.cloudansys.hawkeye.modules.settings.model.vo.PrimaryWarn;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class CacheManager {

    // 存储法兰倾斜告警阈值范围 {提升高度：{提升阶段：{告警等级：告警值}}}
    private static Map<Integer, Map<Integer, Map<Integer, Double>>> flThreshold = new ConcurrentHashMap<>();

    // 阈值告警基本设置信息 key为 psq projectId_serialCode_quotaCode，value是这个quota对应的三个等级的告警集合
    private static Map<String, List<PrimaryWarn>> warnInfo = new ConcurrentHashMap<>();
    // 存储刷新标志时间
    private static Map<String, String> flagTime = new ConcurrentHashMap<>();
    // 设置低频数据标志，默认高频数据流
    private static boolean lowFrequencyFlag = false;

    public static Map<Integer, Map<Integer, Map<Integer, Double>>> getFlThreshold() {
        return flThreshold;
    }

    public static boolean getLowFrequencyFlag() {
        return lowFrequencyFlag;
    }

    public static void setLowFrequencyFlag(boolean flag) {
        lowFrequencyFlag = flag;
    }

    /**
     * 设置缓存中缓存刷新标志时间
     */
    public static void setFlagTimeInCache(String flagTimeInRedis) {
        flagTime.put(Const.HK_TASK_INIT, flagTimeInRedis);
    }

    /**
     * 获取缓存中缓存刷新标志时间
     */
    public static String getFlagTimeInCache() {
        return flagTime.get(Const.HK_TASK_INIT);
    }

    /**
     * @param psq projectId_serialCode_quotaCode
     * @return quota对应的三个等级的告警集合
     */
    public static List<PrimaryWarn> getWarnLevelList(String psq) {
        if (warnInfo != null) {
            return warnInfo.get(psq);
        }
        log.error("阈值告警基本设置信息缓存为空！【CacheManager】");
        return null;
    }

    /**
     * 刷新缓存
     * 刷新策略（比对时间）
     */
    public static void flushCache() {
        init();
    }

    /**
     * 初始化缓存
     */
    public static void init() {
        // 初始化当前轮毂实际高度
        initTs();
        // 初始化振动阈值范围
        initZDThreshold();
        // 初始化平整度复位值
        initEvennessResetValue();
        // 初始化法兰阈值范围
        initFLThreshold();
        initTh();
        // 存储刷新标志时间
        String flagTimeInRedis = getFlagTime();
        setFlagTimeInCache(flagTimeInRedis);
    }

    private static void initTh() {
        // 获取阈值告警基本设置信息
        String strObj = getWarnBaseInfoOfRedis();
        if (strObj == null) {
            log.error("Redis 中阈值告警基本设置信息为空！");
            return;
        }
        // 反序列化解出存储的告警对象
        Object desObj = SerializeUtil.deserialize(strObj.getBytes());
        if (desObj == null) {
            log.error("阈值告警基本设置信息解析失败！");
            return;
        }
        warnInfo = (Map<String, List<PrimaryWarn>>) desObj;
        // 设置叶片角度值
        JedisUtil.set(Const.ANGLE_BLADE, String.valueOf(0.0));
    }

    /**
     * 获取 redis 中存储的阈值告警基本设置信息
     */
    private static String getWarnBaseInfoOfRedis() {
        return JedisUtil.get(Const.HK_SET_PW);
    }

    /**
     * 获取 redis 中存储的阈值告警基本设置信息刷新标值 key
     */
    private static String getFlagTime() {
        return JedisUtil.get(Const.HK_TASK_INIT);
    }

    /**
     * 初始化当前轮毂实际高度，以及平整度存储的上次高度，以及高度传感器的原始高度
     */
    private static void initTs() {
        JedisUtil.set(Const.TS_REDIS, String.valueOf(Const.TS_BASE));
        JedisUtil.set(Const.EVEN_LAST_TS, String.valueOf(Const.TS_BASE));
        JedisUtil.set(Const.LAST_TS_TGT, String.valueOf(2.25));
    }

    /**
     * 初始化振动阈值
     */
    private static void initZDThreshold() {
        for (int sc = 33; sc <= 35; sc++) {
            JedisUtil.set(Const.ZD_TH_MAX + sc, String.valueOf(0.2));
            JedisUtil.set(Const.ZD_TH_MIN + sc, String.valueOf(-0.2));
        }
    }

    /**
     * 初始化平整度复位值
     */
    private static void initEvennessResetValue() {
        for (int sc = 36; sc <= 56; sc++) {
            JedisUtil.set(Const.EVEN_RESET + sc, String.valueOf(0.0));
        }
    }

    /**
     * 获取 redis 中存储的叶片角度值
     */
    private static Double BladeAngle() {
//        String redisKey = "hk:bi:qts:tt:6";
//        String s = JedisUtil.lIndex(redisKey, 2);
//        if (s == null) {
//            return null;
//        }
//        Object obj = SerializeUtil.deserialize(s.getBytes());
//        if (obj == null) {
//            return null;
//        }
//        SettingsDataQuota settingsDataQuota = (SettingsDataQuota) obj;
//        return Double.parseDouble(settingsDataQuota.getName());
        return 0.0;
    }

    /**
     * 初始化法兰倾斜阈值告警范围
     * 每个高度代表一个范围的最大值，例如：提升高度小于 5 米时，使用 5 米所对应的阈值范围
     * 哪个高度下的 哪个项目下的 哪个告警等级下的
     */
    private static void initFLThreshold() {

        // 1、提升高度 5m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th5 = new HashMap<>();
        Map<Integer, Double> th51 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th52 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th5.put(29, th51);
        th5.put(37, th52);
        flThreshold.put(5, th5);

        // 2、提升高度 10m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th10 = new HashMap<>();
        Map<Integer, Double> th101 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th102 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th10.put(29, th101);
        th10.put(37, th102);
        flThreshold.put(10, th10);

        // 3、提升高度 15m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th15 = new HashMap<>();
        Map<Integer, Double> th151 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th152 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th15.put(29, th151);
        th15.put(37, th152);
        flThreshold.put(15, th15);

        // 4、提升高度 20m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th20 = new HashMap<>();
        Map<Integer, Double> th201 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th202 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th20.put(29, th201);
        th20.put(37, th202);
        flThreshold.put(20, th20);

        // 5、提升高度 25m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th25 = new HashMap<>();
        Map<Integer, Double> th251 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th252 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th25.put(29, th251);
        th25.put(37, th252);
        flThreshold.put(25, th25);

        // 6、提升高度 30m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th30 = new HashMap<>();
        Map<Integer, Double> th301 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th302 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th30.put(29, th301);
        th30.put(37, th302);
        flThreshold.put(30, th30);

        // 7、提升高度 35m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th35 = new HashMap<>();
        Map<Integer, Double> th351 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th352 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th35.put(29, th351);
        th35.put(37, th352);
        flThreshold.put(35, th35);

        // 8、提升高度 40m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th40 = new HashMap<>();
        Map<Integer, Double> th401 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th402 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th40.put(29, th401);
        th40.put(37, th402);
        flThreshold.put(40, th40);

        // 9、提升高度 45m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th45 = new HashMap<>();
        Map<Integer, Double> th451 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th452 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th45.put(29, th451);
        th45.put(37, th452);
        flThreshold.put(45, th45);

        // 10、提升高度 50m 时的阈值范围
        Map<Integer, Map<Integer, Double>> th50 = new HashMap<>();
        Map<Integer, Double> th501 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        Map<Integer, Double> th502 = new HashMap<Integer, Double>() {
            {
                put(1, 0.24);
                put(2, 0.18);
                put(3, 0.10);
            }
        };
        th50.put(29, th501);
        th50.put(37, th502);
        flThreshold.put(50, th50);
    }

}
