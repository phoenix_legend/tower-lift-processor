package com.cloudansys.core.flink.sink;

import com.alibaba.fastjson.JSON;
import com.cloudansys.config.DefaultConfig;
import com.cloudansys.core.constant.Const;
import com.cloudansys.core.entity.LiftDataEntity;
import com.cloudansys.core.entity.MultiDataEntity;
import com.cloudansys.core.util.CustomDateUtil;
import com.cloudansys.core.util.MqttUtil;
import com.cloudansys.core.util.SerializeUtil;
import com.cloudansys.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.eclipse.paho.client.mqttv3.MqttClient;
import redis.clients.jedis.Jedis;

import java.util.List;

@Slf4j
public class YBMqttSink extends RichSinkFunction<List<MultiDataEntity>> {

    private static String topic_main;
    private static MqttClient mqttClient;
    private static Jedis jedis;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        jedis = DefaultConfig.getJedis();
        topic_main = DefaultConfig.get(Const.MQTT_TOPIC_MAIN_YL);
        mqttClient = DefaultConfig.getMqttClient();
    }

    /**
     *  把振动传感器原始监测数据推送到 mqtt
     */
    @Override
    public void invoke(List<MultiDataEntity> element, Context context) {
//        log.info("##### 【element】 ##### {}", element);
        for (MultiDataEntity multiDataEntity : element) {
            sendToMqtt(multiDataEntity);
        }
    }

    @Override
    public void close() throws Exception {
        super.close();
        if (jedis != null) {
            jedis.close();
        }
        if (mqttClient != null) {
            mqttClient.disconnect();
            mqttClient.close();
        }
    }

    /**
     * 数据推送
     */
    private void sendToMqtt(MultiDataEntity multiDataEntity) {
        LiftDataEntity targetInfo = getTargetInfo(multiDataEntity);
        if (targetInfo == null) {
            return;
        }
        // topic
        String projectId = String.valueOf(targetInfo.getProjectId());
        String serialCode = targetInfo.getSerialCode();
        String topic = StrUtil.stringsToStr(Const.SLASH, topic_main, projectId, serialCode);
        // 数据
        String jsonString = JSON.toJSONString(targetInfo);
        try {
            // 向 mqtt 推送数据
            MqttUtil.sendToMqtt(mqttClient, topic, jsonString);
//            log.info("##### send to mqtt success #####");
        } catch (Exception e) {
            log.info("##### send to mqtt failed #####");
            e.printStackTrace();
        }
    }

    /**
     * 获取 redis 缓存中对应的传感器信息
     * 【key -> hk:bi:tgt:projectId_serialCode】，
     */
    private LiftDataEntity getTargetInfo(MultiDataEntity multiDataEntity) {
        String projectId = multiDataEntity.getProjectId();
        String serialCode = multiDataEntity.getSerialCode();
        String pickTime = multiDataEntity.getPickTime();
        Double[] values = multiDataEntity.getValues();
        // 获取 redis 缓存中对应的传感器信息
        String tgtKey = "hk:bi:tgt:" + projectId + Const.BAR_BOTTOM + serialCode;
        String s = jedis.get(tgtKey);
        if (s == null) {
            log.error("获取传感器基础信息失败！redisKey 为：{}", tgtKey);
            return null;
        }
        LiftDataEntity liftDataEntity = (LiftDataEntity) SerializeUtil.deserialize(s.getBytes());
        // 更新数据和数据时间
        liftDataEntity.setQuotaValues(values);
        liftDataEntity.setPickTime(CustomDateUtil.formatToStdTime(pickTime));
        return liftDataEntity;
    }

}
