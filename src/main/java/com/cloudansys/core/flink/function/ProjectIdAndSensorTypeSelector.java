package com.cloudansys.core.flink.function;

import com.cloudansys.core.entity.MultiDataEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.java.functions.KeySelector;

import java.util.List;

@Slf4j
public class ProjectIdAndSensorTypeSelector implements KeySelector<List<MultiDataEntity>, String> {

    /**
     * 根据项目 id 和传感器类型分组
     */
    @Override
    public String getKey(List<MultiDataEntity> element) throws Exception {

        // 因为同一个 List<MultiDataEntity> 中的基本信息相同，这里从第一个元素中取出基本信息
        String projectId = element.get(0).getProjectId();
        String targetType = element.get(0).getTargetType();
//        log.info("【KeySelector】");
        return projectId + targetType;
    }
}
