package com.cloudansys.core.flink.function;

import lombok.extern.slf4j.Slf4j;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.util.StringUtils;

@Slf4j
public class NullFilter implements FilterFunction<String> {

    @Override
    public boolean filter(String value) throws Exception {
//        log.info("【rawValue】: {}", value);
        return !StringUtils.isNullOrWhitespaceOnly(value);
    }

}
